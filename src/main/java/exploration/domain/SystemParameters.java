package exploration.domain;

public class SystemParameters {
    private final String data101;
    private final String data102;

    public SystemParameters(String data101, String data102) {
        this.data101 = data101;
        this.data102 = data102;
    }
}
