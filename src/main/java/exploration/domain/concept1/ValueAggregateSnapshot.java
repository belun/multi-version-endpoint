package exploration.domain.concept1;

public class ValueAggregateSnapshot {
    private final String value101;
    private final String value102;
    private final String value103;

    public ValueAggregateSnapshot(String value101, String value102, String value103) {
        this.value101 = value101;
        this.value102 = value102;
        this.value103 = value103;
    }

    public String getValue101() {
        return value101;
    }

    public String getValue102() {
        return value102;
    }

    public String getValue103() { // not exposed in response
        return value103;
    }
}
