package exploration.domain.concept1;

public class ValueAggregate {

    private final String value101;
    private final String value102;
    private final String value103;
    private final String value104; // no used in snapshot

    public ValueAggregate(String value1, String value2, String value3, String value4) {
        this.value101 = value1;
        this.value102 = value2;
        this.value103 = value3;
        this.value104 = value4;
    }

    public ValueAggregate doStuff1() {
        return this;
    }

    public ValueAggregateSnapshot snapshot() {
        return new ValueAggregateSnapshot(value101, value102, value103);
    }
}
