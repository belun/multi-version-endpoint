package exploration.domain.concept2;

import exploration.domain.concept1.ValueAggregate;

import java.util.Collection;

public class ContainerAggregate {

    private final Collection<ValueAggregate> value201;
    private final String value202;

    ContainerAggregate(Collection<ValueAggregate> value1,
                       String value2) {
        this.value201 = value1;
        this.value202 = value2;
    }

    public ContainerAggregate doStuff2() {
        return this;
    }
}
