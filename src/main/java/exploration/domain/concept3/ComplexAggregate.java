package exploration.domain.concept3;

import exploration.domain.concept1.ValueAggregate;
import exploration.domain.concept2.ContainerAggregate;

public class ComplexAggregate {

    private final ContainerAggregate value301;
    private final ValueAggregate value302;
    private final String value303;

    ComplexAggregate(ContainerAggregate value1, ValueAggregate value2, String value3) {
        this.value301 = value1;
        this.value302 = value2;
        this.value303 = value3;
    }

    public ComplexAggregate doStuff3() {
        return this;
    }
}
