package exploration.domain;

import exploration.providers.Service1;
import exploration.providers.Service2;
import exploration.providers.Services;

public class GenericConfiguration {
    public Service1 service1() {
        return new Service1();
    }
    public Service2 service2() {
        return new Service2();
    }
    public Services services() {
        return new Services(service1(), service2());
    }
}
