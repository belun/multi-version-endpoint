package exploration.endpoint.configuration;

import exploration.domain.GenericConfiguration;
import exploration.endpoint.v1.EndpointV1;

public class ConfigurationV1 {

    private final GenericConfiguration genericConfiguration = new GenericConfiguration();

    public EndpointV1 endpointV1() {
        return new EndpointV1(genericConfiguration.services());
    }
}
