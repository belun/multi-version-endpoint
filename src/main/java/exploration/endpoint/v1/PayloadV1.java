package exploration.endpoint.v1;

import exploration.endpoint.v1.data.ContainerComposite;
import exploration.endpoint.v1.data.PrimitiveContainer;
import exploration.endpoint.v1.data.SimpleComposite;

public class PayloadV1 {
    private final String data001;
    private final PrimitiveContainer data002;
    private final SimpleComposite data003;
    private final ContainerComposite data004;

    private PayloadV1(String data001,
                      PrimitiveContainer data002,
                      SimpleComposite data003,
                      ContainerComposite data004) {
        this.data001 = data001;
        this.data002 = data002;
        this.data003 = data003;
        this.data004 = data004;
    }

    public String getData001() {
        return data001;
    }
    public PrimitiveContainer getData002() {
        return data002;
    }
    public SimpleComposite getData003() {
        return data003;
    }
    public ContainerComposite getData004() {
        return data004;
    }

    public static class Builder {
        private String data1;
        private PrimitiveContainer data2;
        private SimpleComposite data3;
        private ContainerComposite data4;

        public Builder withData001(String data) {
            this.data1 = data;
            return this;
        }
        public Builder withData002(PrimitiveContainer data) {
            this.data2 = data;
            return this;
        }
        public Builder withData003(SimpleComposite data) {
            this.data3 = data;
            return this;
        }
        public Builder withData004(ContainerComposite data) {
            this.data4 = data;
            return this;
        }

        public PayloadV1 build() {
            return new PayloadV1(data1, data2, data3, data4);
        }
    }
}
