package exploration.endpoint.v1;

import exploration.domain.concept1.ValueAggregate;
import exploration.domain.concept1.ValueAggregateSnapshot;
import exploration.providers.Services;

public class EndpointV1 {
    private final Services services;

    public EndpointV1(Services services) {
        this.services = services;
    }

    public ResultV1 process1(RequestParametersV1 requestParametersV1, PayloadV1 payloadV1) {
        ValueAggregate concept1 = createConcept1(requestParametersV1, payloadV1);

        concept1 = concept1.doStuff1();
        final ValueAggregateSnapshot snapshot = concept1.snapshot();

        return createResultV1(snapshot);
    }

    private ValueAggregate createConcept1(RequestParametersV1 requestParametersV1,
                                          PayloadV1 payloadV1) {
        return new ValueAggregate(
                payloadV1.getData001(),
                payloadV1.getData002().getPrimitiveValue(),
                payloadV1.getData003().getPrimitiveContainer().getPrimitiveValue(),
                requestParametersV1.getData101());
    }

    private ResultV1 createResultV1(ValueAggregateSnapshot snapshot) {
        return new ResultV1(snapshot.getValue101(), snapshot.getValue102());
    }

}
