package exploration.endpoint.v1;

public class RequestParametersV1 {
    private final String data101;
    private final String data102;
    private final String data103;

    private RequestParametersV1(String data101, String data102, String data103) {
        this.data101 = data101;
        this.data102 = data102;
        this.data103 = data103;
    }

    public String getData101() {
        return data101;
    }
    public String getData102() {
        return data102;
    }
    public String getData103() {
        return data103;
    }

    public static class Builder {
        private String data101;
        private String data102;
        private String data103;

        public Builder withData101(String data) {
            this.data101 = data;
            return this;
        }
        public Builder withData102(String data) {
            this.data102 = data;
            return this;
        }
        public Builder withData103(String data) {
            this.data103 = data;
            return this;
        }

        public RequestParametersV1 build() {
            return new RequestParametersV1(data101, data102, data103);
        }
    }
}
