package exploration.endpoint.v1;

public class ResultV1 {
    private final String value101;
    private final String value102;

    public ResultV1(String value101, String value102) {
        this.value101 = value101;
        this.value102 = value102;
    }
}
