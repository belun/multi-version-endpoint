package exploration.endpoint.v1.data;

public class PrimitiveContainer {
    private final String primitiveValue;

    public PrimitiveContainer(String primitiveValue) {
        this.primitiveValue = primitiveValue;
    }

    public String getPrimitiveValue() {
        return primitiveValue;
    }
}
