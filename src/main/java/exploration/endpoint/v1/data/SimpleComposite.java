package exploration.endpoint.v1.data;

public class SimpleComposite {
    private final String primitiveValue;
    private final PrimitiveContainer primitiveContainer;

    public SimpleComposite(String primitiveValue, PrimitiveContainer primitiveContainer) {
        this.primitiveValue = primitiveValue;
        this.primitiveContainer = primitiveContainer;
    }

    public String getPrimitiveValue() {
        return primitiveValue;
    }

    public PrimitiveContainer getPrimitiveContainer() {
        return primitiveContainer;
    }
}
