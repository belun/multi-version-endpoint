package exploration.endpoint.v1.data;

import java.util.Collection;

public class ContainerComposite {
    private final String primitiveValue;
    private final PrimitiveContainer primitiveContainer;
    private final Collection<SimpleComposite> primitiveContainers;

    public ContainerComposite(String primitiveValue,
                              PrimitiveContainer primitiveContainer,
                              Collection<SimpleComposite> primitiveContainers) {
        this.primitiveValue = primitiveValue;
        this.primitiveContainer = primitiveContainer;
        this.primitiveContainers = primitiveContainers;
    }

    public String getPrimitiveValue() {
        return primitiveValue;
    }

    public PrimitiveContainer getPrimitiveContainer() {
        return primitiveContainer;
    }

    public Collection<SimpleComposite> getPrimitiveContainers() {
        return primitiveContainers;
    }
}
