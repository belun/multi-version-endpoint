package exploration;

import com.google.common.collect.Lists;
import exploration.endpoint.v1.RequestParametersV1;
import exploration.endpoint.v1.PayloadV1;
import exploration.endpoint.configuration.ConfigurationV1;
import exploration.endpoint.v1.ResultV1;
import exploration.endpoint.v1.data.ContainerComposite;
import exploration.endpoint.v1.data.PrimitiveContainer;
import exploration.endpoint.v1.data.SimpleComposite;

public class Client {

    private static ConfigurationV1 configuration = new ConfigurationV1();

    public static void main(String[] args) {
        final PayloadV1 payloadV1 =
            new PayloadV1.Builder()
                .withData001("data001")
                .withData002(new PrimitiveContainer("data002"))
                .withData003(new SimpleComposite("data003", new PrimitiveContainer("data013")))
                .withData004(new ContainerComposite(
                        "data004",
                        new PrimitiveContainer("data014"),
                        Lists.newArrayList(
                                new SimpleComposite("data024", new PrimitiveContainer("data054")),
                                new SimpleComposite("data034", new PrimitiveContainer("data064")),
                                new SimpleComposite("data044", new PrimitiveContainer("data074")))
                    )
                )
                .build();
        final RequestParametersV1 requestParametersV1 =
            new RequestParametersV1.Builder()
                    .withData101("data101")
                    .withData102("data102")
                    .build();

        ResultV1 resultV1 = configuration.endpointV1().process1(requestParametersV1, payloadV1);
    }
}
