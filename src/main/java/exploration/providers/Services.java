package exploration.providers;

public class Services {
    private final Service1 service1;
    private final Service2 service2;

    public Services(Service1 service1, Service2 service2) {
        this.service1 = service1;
        this.service2 = service2;
    }

    public Service1 getService1() {
        return service1;
    }

    public Service2 getService2() {
        return service2;
    }
}
